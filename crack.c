#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *firsth = md5(guess, strlen(guess));
    int match = 0;
    
    int result = strcmp(firsth, hash);
    // Compare the two hashes
    if( result == -10 || result == 0)
    { 
        //printf("%s = %s", firsth, hash);
        printf("result %d\n", result);
        match = 1;
    }
    else 
    {
        
        match = 0;
    }
    
    // Free any malloc'd memory
    free(firsth);

    return match;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    struct stat info;
    if(stat(filename, &info) == -1)
    {
        printf("Can't stat file\n");
        exit(1);
    }
    int filesize = info.st_size;
    printf("File is %d bytes\n", filesize);
    
    //allocate memory
    char *contents = malloc(filesize+1);
    
    FILE *dictionary = fopen(filename ,"rb");
    if (!dictionary)
    {
        printf("Can't open %s for reading\n", filename);
        exit(1);
    }
    
    fread(contents, 1, filesize, dictionary);
    fclose(dictionary);
    
    //add null to the end
    contents[filesize] = '\0';
    
    int lineCount = 0;
    for( int i = 0; i < filesize; i++)
    {
        if(contents[i] == '\n') lineCount++;
    }
    
    printf("num of lines is %d\n", lineCount);
    
    char ** arrStr = malloc( lineCount * sizeof(char*));
    //fill in array
    
    arrStr[0] = strtok(contents, "\n");
    int i = 1;
    while((arrStr[i] = strtok(NULL, "\n"))!= NULL)
    {
        i++;
    }
    
    
    for( int e = 0; e < lineCount; e++)
    {
        //printf("%d %s\n", e, arrStr[e]);
    }
    *size = lineCount;
    free(contents);
    
    
    return arrStr;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    //read_dictionary(filename, dlen);
    char **dict = read_dictionary(argv[2], &dlen);
    free(dict);
    // Open the hash file for reading.
    FILE *hashes = fopen(argv[1], "r");
    if(!hashes)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    

    // For each hash, try every entry in the dictionary.
    char hash1[40];
    while ((fgets(hash1, 40, hashes)!= NULL))
    {
        for(int i = 0; i < dlen; i++)
        {
            int same = tryguess(hash1, dict[i]);
            if(same == 1)
            {
                printf("match here: %s\nhash = %s", dict[i],hash1);    
            }
        }
    }
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    
    fclose(hashes);
}
